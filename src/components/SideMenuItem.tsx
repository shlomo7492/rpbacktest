import React from 'react'
interface menuItemProps {
    type:string;
}
const menuItem:React.FC<menuItemProps> = ({type})=>{
    return <i className={"fas fa-"+type}></i>
}
export default menuItem;