import React, { ReactNode } from 'react'
interface cardProps {
    classname:string;
}
const Card:React.FC<cardProps> = (props)=>{
    return <div className={props.classname}>{props.children}</div>
}

export default Card;