import React from 'react'

const HeaderTitle:React.FC = (props)=>{
    return <h1>{props.children}</h1>
}
export default HeaderTitle;