import React from 'react'
interface MenuItemProps {
    text:string;
    classname:string;
    active:boolean;
}
const MenuItem:React.FC<MenuItemProps> = ({text,active,classname})=>{
    return <div className={active?classname:""}><a href="#" >{text}</a></div>
}
export default MenuItem;