import React from 'react'
import Card from "./Card";
interface CourseType {
    course:Course;
    classes:{Card:string,CardTitle:string,CardButton:string}
}
const lessons:Array<number> = [24,30,19,22,16]
const ViewCourse:React.FC<CourseType> = ({course,classes})=>{
        console.log(course)
        return ( 
            <Card classname={classes.Card}>
                <div className={classes.CardTitle}>
                    <div>
                        <div>
                            {course.course}
                        </div>
                        <div>
                            {lessons[course.id-1]} lessons
                        </div>
                    </div>
                    <div>{course.time}</div>
                </div>
                <div className={classes.CardButton}>&#9654;</div>
            </Card>
        )
 

}

export default ViewCourse;