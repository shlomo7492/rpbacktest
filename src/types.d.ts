type Course = {
    id:number;
    course:string;
    time:string;
}

type Time = {
    id:number;
    time:number;
}