import {makeObservable,observable,action,computed, autorun} from "mobx";
import axios from "axios";


class CoursesStore {
    courses:Array<Course> = [];
    categories:Array<string>=[];

    constructor (){
        makeObservable(this,{
            courses:observable,
            categories:observable,
            getCoursesLength:computed,
            setCategories:action,
            setCourses:action,
            setStateData:action
        })
        
        autorun (async ()=>{
            await this.setStateData();
        })
    }
    get getCoursesLength(){
        return this.courses.length;
    }
   
    getBlocksFromApi= async()=>{
     const response = await axios.get("https://rpback.com/api/games/test_blocks?project_id=2");
     return [...response.data.blocks];
    }
    getCategoriesFromApi = async()=>{
        const response = await axios.get("https://rpback.com/api/games/test_categories?project_id=2");
        return [...response.data.categories];
    }
    getTimesFromApi = async()=>{
        const response = await axios.get("https://rpback.com/api/games/test_minutes?project_id=2");
        return [...response.data.minutes];
    }

    setCategories = (categories:Array<string>)=>{
        categories.map(category=>this.categories.push(category));
    }
    setCourses = (times:Array<Time>,blocks:Array<string>)=>{
        times.forEach((time)=>{
            blocks.forEach((block,index)=>{
                if ((index+1)===time.id) {
                    const course:Course = {
                        id:time.id,
                        course:block,
                        time:time.time+"min"
                    }
                    this.courses.push(course)
                };
            })
        });
    }
    setStateData = async ()=>{
        const blocks:Array<string> = await this.getBlocksFromApi();
        const categories:Array<string> = await this.getCategoriesFromApi();
        const times:Array<Time> = await this.getTimesFromApi();
        this.setCategories(categories);
        this.setCourses(times,blocks);

    }
} 

const coursesStore = new CoursesStore();

export default coursesStore;