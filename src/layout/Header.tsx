import React from 'react'
import Menu from "./HeaderMenu"
import Title from "../components/HeaderTitle";
const Header:React.FC =()=>{
    return (
        <header>
            <Title>Courses</Title>
            <Menu/>
        </header>
    )

}
export default Header;