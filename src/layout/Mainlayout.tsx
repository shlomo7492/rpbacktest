import React from 'react'
import Sidebar from "./Sidebar";
import Header from "./Header";

import ViewCourse from "../components/ViewCourse";
import classes from "./layout.module.scss";

interface MainLayoutProps {
    courses:Array<Course>;
}
const MainLayout:React.FC<MainLayoutProps> = ({courses})=>{
    return(
        <div className={classes.MainLayout}>
            <div className={classes.Main}>
                <Header/>
                <main>
                    { courses.map(course=><ViewCourse key={course.id} 
                                                      course={{...course}}
                                                      classes={{Card:classes.Card,CardTitle:classes.CardTitle,CardButton:classes.CardButton}}/>)
                    }
                    
                </main>
            </div>
            <Sidebar classname={{Sidebar:classes.Sidebar, Active:classes.Active}}/>
        </div>
    )
}

export default MainLayout;