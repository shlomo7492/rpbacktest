import React from 'react'
import menuItem from '../components/SideMenuItem'
import SideMenuItem from "../components/SideMenuItem"
interface SidebarProps {
    classname:{
        Sidebar:string;
        Active:string;
    };
}
const menuItemsMockArray:Array<string> = ["th-list","clipboard-list", "users"]
const Sidebar:React.FC<SidebarProps> = ({classname})=>{
    return (
        <div className={classname.Sidebar}>
            <div>
                
                {menuItemsMockArray.map((icon,index)=>{
                    return (
                        <a href="#" key={index} className={index<1? classname.Active:""}>
                            <SideMenuItem type={icon} />
                        </a>
                    )}
                )}
                
            </div>
        </div>
    )
}

export default Sidebar;