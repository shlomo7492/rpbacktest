import React from 'react'
import { observer } from 'mobx-react';
import CoursesStore from "../store/CoursesStore";

import MenuItem from "../components/HeaderMenuItem";
import classes from "./layout.module.scss";

const Menu = observer(()=>{
    return (
        <div>
            {CoursesStore.categories.map((category,index)=><MenuItem key={category} 
                                                                    text={category} 
                                                                    active={index<1?true:false}
                                                                    classname={classes.Active}/>)}
        </div>
    )
})
export default Menu;