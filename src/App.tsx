import React from 'react';
import './App.css';
import {observer} from "mobx-react";
import CoursesStore from "./store/CoursesStore"
import MainLayout from "./layout/MainLayout"
//TODO: To scale images and in the cards... May be...

const App:React.FC = observer(() => {
  return (
    <MainLayout courses={[...CoursesStore.courses]}/>
  );
})

export default App;
